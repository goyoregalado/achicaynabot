# AchicaynaBot

Este repositorio almacena el código fuente de un bot de telegram simple
denominado achicaynabot.
Su única función es analizar los mensajes que recibe dentro de aquellos
chats en los que participa y detectar URL's

Este bot es parte de una serie de artículos sobre el desarrollo de bots
de Telegram.

Siéntete libre de utilizar este código para cualquier proyecto personal
pero recuerda, no puedo darte ninguna garantía sobre su funcionamiento,
esto es sólo un proyecto divulgativo.

